"""
WSGI config for CodeWiz project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os, sys
sys.path.append('C:/Users/CJR/Bitnami Django Stack projects/CodeWiz')
os.environ.setdefault("PYTHON_EGG_CACHE", "C:/Users/CJR/Bitnami Django Stack projects/CodeWiz/egg_cache")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "CodeWiz.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
