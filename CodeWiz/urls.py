from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.debug import default_urlconf

urlpatterns = patterns('',
    url(r'', include('users.urls'), name='home'),
    url(r'^wizards/', include("wizards.urls"), name="wizards"),
    url(r'^admin/', include(admin.site.urls), name="admin"),
)
