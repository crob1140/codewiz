from django import forms
from django.contrib.auth.models import User
from users.validators import email_is_available, user_exists, passwords_match

class RegistrationForm(forms.ModelForm):
	password = forms.CharField(max_length = 20, widget = forms.PasswordInput(attrs = {"class" : "form-input", "placeholder" : "Password"}), required = True)
	confirm_password = forms.CharField(max_length = 20, widget = forms.PasswordInput(attrs = {"class" : "form-input", "placeholder" : "Confirm Password"}), required = True)
	
	class Meta:
		model = User
		fields = ["username", "email", "first_name", "last_name"]
	
	def __init__(self, *args, **kwargs):
		super(RegistrationForm, self).__init__(*args, **kwargs)
		# Set required fields
		self.fields["username"].required = True
		self.fields["email"].required = True
		self.fields["confirm_password"].required = True
		
		# Set widget attributes
		self.fields["username"].widget.attrs = {"class" : "form-input", "placeholder" : "Username"}
		self.fields["email"].widget.attrs = {"class" : "form-input", "placeholder" : "Email"}
		
		# Set custom error messages
		self.fields["username"].error_messages = {"invalid" : "This username has already been taken.", "required" : "This field is required."}
	def clean_email(self):
		email = self.cleaned_data["email"]
		if email_is_available(email):
			return email
	def clean_confirm_password(self):
		password = self.cleaned_data["password"]
		confirm_password = self.cleaned_data["confirm_password"]
		if passwords_match(password, confirm_password):
			return confirm_password
			
class LoginForm(forms.Form):
	username = forms.CharField(max_length=20, widget=forms.TextInput(attrs={"class" : "form-input", "placeholder" : "Username"}), required=True)
	password = forms.CharField(max_length=20, widget=forms.PasswordInput(attrs={"class" : "form-input", "placeholder" :"Password"}), required=True)
	def __init__(self, *args, **kwargs):
		super(LoginForm, self).__init__(*args, **kwargs)
		self.fields['username'].error_messages = {
			'required' : "Please ensure your username has been entered before submitting."
		}
		self.fields['password'].error_messages = {
			'required' : "Please ensure your password has been entered before submitting." 
		}
	def clean(self):
		cleaned_data = super(LoginForm, self).clean()
		username = cleaned_data.get('username')
		password = cleaned_data.get('password')
		if user_exists(username, password):
			return cleaned_data