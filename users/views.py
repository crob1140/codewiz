from django.shortcuts import render
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test

from users.forms import RegistrationForm, LoginForm

def register(request):
	context = {}
	if request.method == "POST":
		user_form = RegistrationForm(request.POST)
		
		# Attach the user form to the context, so that a failed registration will re-load the view with these fields and error messages 
		context['user_form'] = user_form
		if user_form.is_valid():
			username = user_form.cleaned_data["username"]
			password = user_form.cleaned_data["password"]
			
			# Create a User model from the form, but do not commit until the password is hashed and salted
			new_user = user_form.save(commit=False) 
			new_user.set_password(password)
			new_user.save()
		
			# TODO: this block handles auto-login and should be removed when email verification is added 
			new_user = auth.authenticate(username=username, password=password)
			if new_user is not None and new_user.is_active: 
				auth.login(request, new_user)
				return HttpResponseRedirect(reverse("dashboard"))
			else:
				return HttpResponseRedirect(reverse("home"))
	else:
		# Return a blank registration form for GET requests
		context['user_form'] = RegistrationForm()
	return render(request, "registration.html", context)
	
def login(request):
	context = {}
	if request.method == "POST":
		login_form = LoginForm(request.POST)
		
		# Attach the log-in form to the context so that a failed log-in will reload the view with these fields and error messages
		context["login_form"] = login_form
		if login_form.is_valid():
			username = login_form.cleaned_data["username"]
			password = login_form.cleaned_data["password"]
			user = auth.authenticate(username=username, password=password)
			if user is not None:
				auth.login(request, user)
				
				redirect_to = reverse("dashboard")
				if "next" in request.POST:
					redirect_to = request.POST["next"]
				return HttpResponseRedirect(redirect_to)
	else:
		context["login_form"] = LoginForm()
		if "next" in request.GET:
			context["next"] = request.GET["next"]
	return render(request, "login.html", context)

def logout(request):
	auth.logout(request)
	return HttpResponseRedirect(reverse("home")) # returns the user to the sites index page 
