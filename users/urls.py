from django.conf.urls import patterns, url
from users import views
from wizards.views import list_wizards

urlpatterns = patterns('',
	url(r'^register/', views.register, name="register"),
	url(r'^login/', views.login, name="login"),
	url(r'^logout/', views.logout, name="logout"),
	url(r'^dashboard/', list_wizards, name="dashboard") # TODO: replace this with a proper dashboard
)