from django.core.exceptions import ValidationError
from django.contrib.auth.models import User 

def email_is_available(email):
	if User.objects.filter(email=email).exists():
		raise ValidationError("This email has already been used")
	return True

def user_exists(username, password):
	# Treat all log-in errors with the same error message, to make it harder for attackers to determine which field is incorrect
	try:
		user = User.objects.get(username=username)
		if (not user.is_active) or (not user.check_password(password)):
			raise User.DoesNotExist 
	except User.DoesNotExist:
		raise ValidationError("Username or password is incorrect", code='invalid')
	return True

def passwords_match(password, confirm_password):
	if password != confirm_password:
		raise ValidationError("Passwords do not match", code='invalid')
	return True
