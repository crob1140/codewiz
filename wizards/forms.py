from django import forms
from widgets import AceWidget, AttributedOptionSelect
from wizards.models import ProgrammingLanguage, Wizard

ACE_MODES = {
	ProgrammingLanguage.JAVASCRIPT : "javascript",
	ProgrammingLanguage.PYTHON : "python"
}

class WizardCreationForm(forms.ModelForm):
	class Meta:
		model = Wizard
		fields = ["name", "code", "language"]
		widgets = {
			"code" : AceWidget(mode="javascript", theme="clouds", attrs={"style" : "height:200px"}),
			"language" : AttributedOptionSelect(choices=ProgrammingLanguage.OPTIONS, option_attrs = {lang : {"ace-mode" : ACE_MODES[lang]} for lang in ACE_MODES})
		}
		
	def __init__(self, *args, **kwargs):
		super(WizardCreationForm, self).__init__(*args, **kwargs)
		self.fields["name"].widget.attrs = {"class" : "form-input", "placeholder" : "Name"}
	
	# TODO: the syntax NEEDS to be validated before the code can be saved, but without using a language server since this communication is asynchronous
	# Need to find a way to check ONLY for syntax errors either inside a Django validator or client side