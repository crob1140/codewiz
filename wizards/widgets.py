from django.utils.safestring import mark_safe
from django.forms import Select
from django.forms.widgets import Widget, Select

class AttributedOptionSelect(Select):
	def __init__(self, *args, **kwargs):
		if "option_attrs" in kwargs:
			self.option_attrs = kwargs["option_attrs"];
			del kwargs["option_attrs"]
		else:
			self.option_attrs = {}
	
		super(AttributedOptionSelect, self).__init__(*args, **kwargs)
		
	
	def render_option(self, selected_choices, option_value, option_label):
		option_selected = ""
		if option_value in selected_choices:
			option_selected = "selected"
		
		if option_value in self.option_attrs:
			option_attrs = " ".join([attr+"=\"" + self.option_attrs[option_value][attr] + "\"" for attr in self.option_attrs[option_value]])
		else:
			option_attrs = ""
			
		html = "<option value=\"" + option_value + "\" " + option_attrs + " " + option_selected + " />" + option_label + "</option>"
		
		return mark_safe(html)
	
class AceWidget(Widget):
	class Media:
		css = {
			'all' : ["css/ace-widget.css"]
		}
		js = ["js/ace-builds/src-min-noconflict/ace.js"]
	
	def __init__(self, *args, **kwargs):
		if "theme" in kwargs:
			self.theme = kwargs["theme"];
			del kwargs["theme"];
		else:
			self.theme = "cloud";
		
		if "mode" in kwargs:
			self.mode = kwargs["mode"];
			del kwargs["mode"];
		else:
			self.mode = "javscript";

		super(AceWidget, self).__init__(*args, **kwargs);
	
	def render(self, name, value, attrs=None):
		# Determine a way to identify the container object, using the "id" attribute if it was provided and the name otherwise
		id = name
		if attrs and "id" in attrs:
			id = attrs["id"];
		if value is None:
			value = ""
		
		# Use this identifier to generate ids for the individual components so that they can also be accessed from JavaScript
		hiddenValId = id + "_val";
		self.__editor_id = id + "_ace";
		
		# Convert all the widget attributes to a list of HTML attributes
		containerAttrs = self.attrs.copy();
		if attrs is not None:
			containerAttrs.update(attrs);
		containerAttrs = " ".join([attr+"=\""+containerAttrs[attr]+"\"" for attr in containerAttrs]);
			
		# Generate a HTML blob for the entire widget using the variables set above
		html = "\n".join(["",
				"<div " + containerAttrs + ">"
				"	<input type=\"hidden\" name=\"" + name + "\"  value=\"" + value + "\" id=\"" + hiddenValId + "\" />",
				"	<div class=\"ace-code\" id=\"" + self.__editor_id + "\">" + value + "</div>",
				"	<script>",
				"		var input = document.getElementById(\"" + hiddenValId + "\");",
				"		var code = ace.edit(\"" + self.__editor_id + "\");",
				"		code.setTheme(\"ace/theme/" + self.theme + "\");",
				"		code.getSession().setMode(\"ace/mode/" + self.mode + "\");",
				"		code.getSession().on('change', function(e){",
				"			input.value = code.getSession().getValue();",
				"		});",
				"	</script>",
				"</div>"]);
				
		return mark_safe(html);
		