from django.contrib.auth.models import User
from django.db import models

# Programming languages are only used for application logic, so they don't need to be saved as a model in the db
class ProgrammingLanguage:
	# Define some enumerables for the various supported languages
	# There will never be more than 99 languages supported, so 2 characters will be enough
	JAVASCRIPT = "JS" 
	PYTHON = "PY"
	
	VERBOSE_NAMES = {
		JAVASCRIPT : "JavaScript",
		PYTHON : "Python"
	}
	
	OPTIONS = [
		(JAVASCRIPT, VERBOSE_NAMES[JAVASCRIPT]),
		(PYTHON, VERBOSE_NAMES[PYTHON])
	]

class Wizard(models.Model):
	id = models.AutoField(primary_key=True)
	owner = models.ForeignKey(User, related_name="wizards", null = False)
	name = models.CharField(max_length = 20, null = False, blank = False)
	code = models.TextField(null = False, blank = False)
	language = models.CharField(max_length = 2, choices = ProgrammingLanguage.OPTIONS, null = False, blank = False, default = ProgrammingLanguage.JAVASCRIPT)