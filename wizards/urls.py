from django.conf.urls import patterns, url
from wizards import views

urlpatterns = patterns('',
	url(r'^$', views.list_wizards, name="list_wizards"),
	url(r'(?P<wizard_id>\d+)$', views.view_details, name="wizard_details"),
	url(r'^create/', views.create_wizard, name="create_wizard"),
)