from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect
from wizards.models import Wizard, ProgrammingLanguage
from wizards.forms import WizardCreationForm

@login_required
def list_wizards(request):
	context = {
		"wizards" : [wizard for wizard in request.user.wizards.all()]
	}
	return render(request, 'list.html', context)

@login_required	
def view_details(request, wizard_id):
	wizard = get_object_or_404(Wizard, id=wizard_id);
	context = {
		"wizard" : {
			"name" : wizard.name,
			"code" : wizard.code,
			"language" : ProgrammingLanguage.VERBOSE_NAMES[wizard.language]
		}
	}
	return render(request, 'details.html', context)

@login_required	
def create_wizard(request):
	context = {}
	if request.method == "POST":
		creation_form = WizardCreationForm(request.POST)
		context["creation_form"] = creation_form
		if creation_form.is_valid():
			new_wizard = creation_form.save(commit=False)
			new_wizard.owner = request.user
			new_wizard.save()
			return HttpResponseRedirect(reverse("list_wizards"))
	else:
		context["creation_form"] = WizardCreationForm()
	return render(request, 'create.html', context)